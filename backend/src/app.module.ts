import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

// routes
import { RouterModule } from 'nest-router';
import { routes } from './app.routes';
import { AuthModule } from './modules/auth/auth.module';

@Module({
  imports: [RouterModule.forRoutes(routes), AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
