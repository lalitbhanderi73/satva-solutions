import { Routes } from 'nest-router';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
const { API_BASE } = process.env;
import { AuthModule } from './modules/auth/auth.module';

export const routes: Routes = [
  {
    path: `${API_BASE}/auth`,
    module: AuthModule,
  },
];
