import { Module } from '@nestjs/common';
import { AppConstants } from 'src/constants/app.constant';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { LocalStrategy } from './strategy/local.strategy';
import { OptionalStrategy } from './strategy/optional.strategy';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: AppConstants.jwtSecret,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy, OptionalStrategy],
})
export class AuthModule {}
