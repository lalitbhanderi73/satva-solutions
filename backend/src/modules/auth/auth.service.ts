import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserLoginDTO } from './dto/UserLogin.dto';
import { JwtUserDTO } from './dto/JwtUser.dto';
import { UserAuthenticateInterface } from './interfaces/userAuthenticate.interface';
import { UserTransactionDTO } from './dto/userTransaction.dto';
import { AES, enc } from 'crypto-js';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  async login(userLoginDTO: UserLoginDTO) {
    const userAuthenticateInterface: UserAuthenticateInterface = {
      email: userLoginDTO.email,
      password: userLoginDTO.password,
    };
    const email = 'admin@example.com',
      password = '123456';
    if (
      userAuthenticateInterface.email == email &&
      userAuthenticateInterface.password == password
    ) {
      return userAuthenticateInterface;
    } else {
      throw new BadRequestException();
    }
  }

  async signJWT(user: JwtUserDTO) {
    const payload = {
      id: user.id,
      email: user.email,
    };

    const token = this.jwtService.sign(payload);
    return token;
  }

  async transaction(
    jwtUserDto: JwtUserDTO,
    userTransactionDTO: UserTransactionDTO,
  ) {
    const plaintext = userTransactionDTO.processingCode;
    const secretKey = 'satva';

    const ciphertext = AES.encrypt(plaintext, secretKey).toString();

    const decrtext = AES.decrypt(ciphertext, secretKey).toString(enc.Utf8);
    console.log('ciphertext', ciphertext);
    console.log('decrtext', decrtext);

    //const hash = bcrypt.hashSync(userRegisterDTO.password, 10);
    console.log(userTransactionDTO);

    return {
      approvalCode: Math.random().toString().substr(2, 6),
      dateTime: new Date().toUTCString(),
    };
  }
}
