import { AuthGuard } from '@nestjs/passport';
import {
  Controller,
  Post,
  UseGuards,
  Request as RequestDec,
  HttpStatus,
  Body,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { IResponse } from 'src/common/interfaces/response.interface';
import { ResponseSuccess } from 'src/common/dto/response.dto';
import { UserTransactionDTO } from './dto/userTransaction.dto';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@RequestDec() req): Promise<IResponse> {
    const user: any = req.user;
    const access_token = await this.authService.signJWT(user);
    return new ResponseSuccess(HttpStatus.OK, 'Success', { access_token });
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('transaction')
  async transaction(
    @Body() transaction: UserTransactionDTO,
    @RequestDec() req,
  ): Promise<IResponse> {
    const isCreated = await this.authService.transaction(req.user, transaction);
    return new ResponseSuccess(HttpStatus.CREATED, 'Success', isCreated);
  }
}
