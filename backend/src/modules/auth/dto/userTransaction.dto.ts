import { IsNotEmpty } from 'class-validator';

export class UserTransactionDTO {
  @IsNotEmpty()
  processingCode: string;

  @IsNotEmpty()
  systemTraceNr: string;

  @IsNotEmpty()
  functionCode: string;

  @IsNotEmpty()
  cardNo: string;

  @IsNotEmpty()
  cardHolder: string;

  @IsNotEmpty()
  amountTrxn: string;

  @IsNotEmpty()
  currencyCode: string;
}
