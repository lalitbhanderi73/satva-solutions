export interface UserAuthenticateInterface {
  email: string;
  password: string;
}
