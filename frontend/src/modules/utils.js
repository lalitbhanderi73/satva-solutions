import { toast } from "react-toastify";

export const AUTH_KEY = "accessToken";

export const setAuthToken = (token = "") =>
  localStorage.setItem(AUTH_KEY, token);

export const getAuthToken = () => localStorage.getItem(AUTH_KEY);

export const removeAuthToken = () => localStorage.removeItem(AUTH_KEY);

export const removeLocalStorageItem = (key) => localStorage.removeItem(key);

export const successToast = (msg) => toast.success(msg);

export const errorToast = (msg) => toast.error(msg);

export const numberValidation = (value) =>
  !isNaN(value) && value !== "-" ? true : false;
