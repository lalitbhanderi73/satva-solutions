import "./App.css";
import { ToastContainer } from "react-toastify";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import reduxThunk from "redux-thunk";
import rootReducer from "./redux";
import Login from "./pages/Login";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Transaction } from "./pages/Transaction";
import Header from "./components/Header";

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);

const store = createStoreWithMiddleware(rootReducer);

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <Routes>
          <Route exact path="/" element={<Login />}></Route>
          <Route exact path="/transaction" element={<Transaction />}></Route>
        </Routes>
      </Router>
      <ToastContainer
        style={{ width: "370px" }}
        autoClose={5000}
        bodyClassName="p_toast_body"
        className="p_toast_wrapper"
        pauseOnFocusLoss={false}
        closeOnClick={false}
        pauseOnHover
        draggable
        newestOnTop
        position="top-right"
        progressClassName="p_toast_progress"
        rtl={false}
        toastClassName="p_toast"
      />
    </Provider>
  );
}

export default App;
