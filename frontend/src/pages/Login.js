import React from "react";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { fetchLoginAction } from "../redux/actions/Login";
import { loginFormSchema } from "../components/FormValidator";
import ErrorMessage from "../components/ErrorMessage";
import { setAuthToken, successToast } from "../modules/utils";
import { useNavigate } from "react-router-dom";

export const loginFields = {
  username: "admin@example.com",
  password: "123456",
};

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { loading = false } = useSelector((state) => ({
    loading: state?.loginData?.loading,
  }));

  const handleLogin = (values, { resetForm }) => {
    dispatch(fetchLoginAction(values)).then((res) => {
      if (res?.message === "Success") {
        setAuthToken(res?.data?.access_token);
        successToast(res?.message);
        navigate("/transaction");
        resetForm();
      }
    });
  };

  return (
    <Formik
      initialValues={loginFields}
      validationSchema={loginFormSchema}
      onSubmit={handleLogin}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        submitCount,
      }) => {
        const showMsgError = (key) => (
          <ErrorMessage
            errors={errors}
            submitCount={submitCount}
            fieldName={key}
          />
        );
        return (
          <>
            <div className="mx-auto main-div p-5 mt-5 border">
              <div class="mb-3 mt-5 ">
                <h3 className="text-center mb-0">Login </h3>
                <label for="exampleFormControlInput1" class="form-label">
                  Email
                </label>
                <input
                  type="email"
                  class="form-control"
                  name="username"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values?.username}
                  placeholder="Enter Email"
                />
                {showMsgError("username")}
              </div>
              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">
                  Password
                </label>
                <input
                  type="password"
                  class="form-control"
                  value={values?.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="password"
                  placeholder="Enter Password"
                />
                {showMsgError("password")}
              </div>
              <button
                type="button"
                onClick={handleSubmit}
                className="btn btn-primary"
                disabled={loading}
              >
                Login{" "}
                {loading && (
                  <div class="spinner-border spinner-border-sm " role="status">
                    <span class="visually-hidden">Loading...</span>
                  </div>
                )}
              </button>
            </div>
          </>
        );
      }}
    </Formik>
  );
};

export default Login;
