import { Formik } from "formik";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import DialogModal from "../components/DialogModal";
import ErrorMessage from "../components/ErrorMessage";
import { transactionFormSchema } from "../components/FormValidator";
import { numberValidation, successToast } from "../modules/utils";
import { fetchTransactionAction } from "../redux/actions/Transaction";

export const transactionFields = {
  cardHolder: "",
  processingCode: "",
  systemTraceNr: "",
  functionCode: "",
  cardNo: "",
  amountTrxn: "",
  currencyCode: "",
};

export const Transaction = () => {
  const [isOpenModal, setIsOpenModal] = useState(false);

  const toggleTransactionModal = () => setIsOpenModal(!isOpenModal);

  const { loading = false, transactionData = {} } = useSelector((state) => ({
    loading: state?.transactionData?.loading,
    transactionData: state?.transactionData?.data,
  }));
  const dispatch = useDispatch();

  const handleTransaction = (values, { resetForm }) => {
    dispatch(fetchTransactionAction(values)).then((res) => {
      if (res?.statusCode === 201) {
        successToast(res?.data?.message);
        toggleTransactionModal();
        resetForm();
      }
    });
  };

  return (
    <>
      <Formik
        initialValues={transactionFields}
        validationSchema={transactionFormSchema()}
        onSubmit={handleTransaction}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          values,
          errors,
          submitCount,
        }) => {
          const showMsgError = (key) => (
            <ErrorMessage
              errors={errors}
              submitCount={submitCount}
              fieldName={key}
            />
          );
          return (
            <div className="mx-auto w-50 p-5">
              <h4>Transaction Form</h4>

              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">
                  Processing Code
                </label>
                <input
                  type="text"
                  class="form-control"
                  value={values?.processingCode}
                  onChange={(e) => {
                    if (numberValidation(e?.target?.value)) {
                      setFieldValue("processingCode", e?.target?.value);
                    }
                  }}
                  onBlur={handleBlur}
                  name="processingCode"
                  placeholder="Enter Processing Code"
                />
                {showMsgError("processingCode")}
              </div>
              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">
                  System Trace No
                </label>
                <input
                  type="text"
                  class="form-control"
                  value={values?.systemTraceNr}
                  onChange={(e) => {
                    if (numberValidation(e?.target?.value)) {
                      setFieldValue("systemTraceNr", e?.target?.value);
                    }
                  }}
                  onBlur={handleBlur}
                  name="systemTraceNr"
                  placeholder="Enter System Trace No"
                />
                {showMsgError("systemTraceNr")}
              </div>
              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">
                  Function Code
                </label>
                <input
                  type="text"
                  class="form-control"
                  value={values?.functionCode}
                  onChange={(e) => {
                    if (numberValidation(e?.target?.value)) {
                      setFieldValue("functionCode", e?.target?.value);
                    }
                  }}
                  onBlur={handleBlur}
                  name="functionCode"
                  placeholder="Enter Function Code"
                />
                {showMsgError("functionCode")}
              </div>
              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">
                  Card No
                </label>
                <input
                  type="text"
                  class="form-control"
                  value={values?.cardNo}
                  onChange={(e) => {
                    if (numberValidation(e?.target?.value)) {
                      setFieldValue("cardNo", e?.target?.value);
                    }
                  }}
                  onBlur={handleBlur}
                  name="cardNo"
                  placeholder="Enter Card No"
                />
                {showMsgError("cardNo")}
              </div>
              <div>
                <label for="exampleFormControlInput1" class="form-label">
                  Card Holder
                </label>
                <input
                  type="text"
                  class="form-control"
                  name="cardHolder"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values?.cardHolder}
                  placeholder="Enter Card Holder"
                />
                {showMsgError("cardHolder")}
              </div>
              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">
                  Amount Transaction
                </label>
                <input
                  type="text"
                  class="form-control"
                  value={values?.amountTrxn}
                  onChange={(e) => {
                    if (numberValidation(e?.target?.value)) {
                      setFieldValue("amountTrxn", e?.target?.value);
                    }
                  }}
                  onBlur={handleBlur}
                  name="amountTrxn"
                  placeholder="Enter Amount Transaction"
                />
                {showMsgError("amountTrxn")}
              </div>
              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">
                  Currency Code
                </label>
                <input
                  type="text"
                  class="form-control"
                  value={values?.currencyCode}
                  onChange={(e) => {
                    if (numberValidation(e?.target?.value)) {
                      setFieldValue("currencyCode", e?.target?.value);
                    }
                  }}
                  onBlur={handleBlur}
                  name="currencyCode"
                  placeholder="Enter Currency Code"
                />
                {showMsgError("currencyCode")}
              </div>
              <button
                type="button"
                onClick={handleSubmit}
                className="btn btn-primary"
                disabled={loading}
              >
                Submit
                {loading && (
                  <div class="spinner-border spinner-border-sm " role="status">
                    <span class="visually-hidden">Loading...</span>
                  </div>
                )}
              </button>
            </div>
          );
        }}
      </Formik>
      {isOpenModal && (
        <DialogModal
          isOpen={isOpenModal}
          toggleFunction={toggleTransactionModal}
          title={"Transaction Details"}
          message={transactionData}
          handleFunction={toggleTransactionModal}
        />
      )}
    </>
  );
};
