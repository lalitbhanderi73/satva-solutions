import {
  FETCH_LOGIN_FAILURE,
  FETCH_LOGIN_REQUEST,
  FETCH_LOGIN_SUCCESS,
} from "../../types/Login";
import { postApi } from "../../api";
import { errorToast } from "../../../modules/utils";

export const fetchLoginRequest = () => ({
  type: FETCH_LOGIN_REQUEST,
});

export const fetchLoginSuccess = (data) => ({
  type: FETCH_LOGIN_SUCCESS,
  data,
});

export const fetchLoginFailure = (error) => ({
  type: FETCH_LOGIN_FAILURE,
  error,
});

export const fetchLoginAction = (data) => (dispatch) => {
  dispatch(fetchLoginRequest());
  return postApi(`auth/login`, data)
    .then((res) => {
      dispatch(fetchLoginSuccess(res?.data?.data));
      return res?.data ?? null;
    })
    .catch((e) => {
      dispatch(fetchLoginFailure(e));
      errorToast(e?.response?.data?.message);
    });
};
