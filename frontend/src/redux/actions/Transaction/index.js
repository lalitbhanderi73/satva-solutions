import {
  FETCH_TRANSACTION_FAILURE,
  FETCH_TRANSACTION_REQUEST,
  FETCH_TRANSACTION_SUCCESS,
} from "../../types/Transaction";
import { postApi } from "../../api";
import { errorToast } from "../../../modules/utils";

export const fetchTransactionRequest = () => ({
  type: FETCH_TRANSACTION_REQUEST,
});

export const fetchTransactionSuccess = (data) => ({
  type: FETCH_TRANSACTION_SUCCESS,
  data,
});

export const fetchTransactionFailure = (error) => ({
  type: FETCH_TRANSACTION_FAILURE,
  error,
});

export const fetchTransactionAction = (data) => (dispatch) => {
  dispatch(fetchTransactionRequest());
  return postApi(`auth/transaction`, data)
    .then((res) => {
      dispatch(fetchTransactionSuccess(res?.data?.data));
      return res?.data ?? null;
    })
    .catch((e) => {
      dispatch(fetchTransactionFailure(e));
      errorToast(e?.response?.data?.message);
    });
};
