import {
  FETCH_LOGIN_FAILURE,
  FETCH_LOGIN_REQUEST,
  FETCH_LOGIN_SUCCESS,
} from "../../types/Login";

const initialState = {
  loading: false,
  data: null,
  error: null,
};

const login = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_LOGIN_SUCCESS:
      return {
        loading: false,
        data: action.data,
        error: "",
      };
    case FETCH_LOGIN_FAILURE:
      return {
        loading: false,
        data: [],
        error: action.data,
      };
    default:
      return state;
  }
};

export default login;
