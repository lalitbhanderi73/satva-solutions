import {
  FETCH_TRANSACTION_FAILURE,
  FETCH_TRANSACTION_REQUEST,
  FETCH_TRANSACTION_SUCCESS,
} from "../../types/Transaction";

const initialState = {
  loading: false,
  data: null,
  error: null,
};

const transaction = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRANSACTION_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_TRANSACTION_SUCCESS:
      return {
        loading: false,
        data: action.data,
        error: "",
      };
    case FETCH_TRANSACTION_FAILURE:
      return {
        loading: false,
        data: [],
        error: action.data,
      };
    default:
      return state;
  }
};

export default transaction;
