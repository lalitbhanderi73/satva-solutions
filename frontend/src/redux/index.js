import { combineReducers } from "redux";
import login from "./reducers/login";
import transaction from "./reducers/transaction";

const rootReducer = combineReducers({
  loginData: login,
  transactionData: transaction,
});

export default rootReducer;
