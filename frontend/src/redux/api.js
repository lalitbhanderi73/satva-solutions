import axios from "axios";
import config from "../../src/config";
import { AUTH_KEY, getAuthToken } from "../modules/utils";

const { API_URL } = config;
export const apiConfig = () => {
  const accessToken = getAuthToken(AUTH_KEY) || "";

  const appConfigData = {
    method: "PUT,DELETE,POST,GET,OPTION",
    headers: {
      accept: "application/json",
    },
  };
  if (accessToken)
    appConfigData.headers.Authorization = `Bearer ${accessToken}`;

  return appConfigData;
};

export const getApi = (url) => axios.get(`${API_URL}${url}`, apiConfig());

export const postApi = (url, apiData) =>
  axios.post(`${API_URL}${url}`, apiData, apiConfig());

export const putApi = (url, apiData) =>
  axios.put(`${API_URL}${url}`, apiData, apiConfig());

export const patchApi = (url, apiData) =>
  axios.patch(`${API_URL}${url}`, apiData, apiConfig());

export const deleteApi = (url) => axios.delete(`${API_URL}${url}`, apiConfig());
