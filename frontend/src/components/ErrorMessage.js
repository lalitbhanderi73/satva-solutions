import React from "react";
const ErrorMessage = ({
  errors = {},
  submitCount = 0,
  fieldName = "",
  fieldName2 = "",
}) => {
  if (errors[fieldName] !== undefined && submitCount) {
    if (fieldName2) {
      return (
        <div className="text-danger">{errors?.[fieldName]?.[fieldName2]}</div>
      );
    }
    return (
      <div className="custom_req">
        <div className="text-danger">{errors?.[fieldName]}</div>
      </div>
    );
  }
  return null;
};

export default ErrorMessage;
