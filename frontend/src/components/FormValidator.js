import * as Yup from "yup";

export const loginFormSchema = () =>
  Yup.object().shape({
    username: Yup.string()
      .required("Please enter email address.")
      .email("invalidEmail"),
    password: Yup.string().required("Please enter password."),
  });

export const transactionFormSchema = () =>
  Yup.object().shape({
    cardHolder: Yup.string().required("required"),
    processingCode: Yup.number("enter only number").required("required."),
    systemTraceNr: Yup.number("enter only number").required("required."),
    functionCode: Yup.number("enter only number").required("required."),
    cardNo: Yup.number("enter only number").required("required."),
    amountTrxn: Yup.number("enter only number").required("required."),
    currencyCode: Yup.number("enter only number").required("required."),
  });
