import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { removeAuthToken, successToast } from "../modules/utils";

const Header = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const handleData = () => {
    if (location?.pathname === "/transaction") {
      removeAuthToken();
      navigate("/");
      successToast("Logout successfully!");
    }
  };
  return (
    <header class="p-3 text-bg-dark">
      <div class="container">
        <div class="text-end">
          <button
            type="button"
            class="btn btn-outline-light me-2"
            onClick={handleData}
          >
            {location?.pathname === "/" ? "Login" : "Logout"}
          </button>
        </div>
      </div>
    </header>
  );
};

export default Header;
