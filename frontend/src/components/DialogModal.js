import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const DialogModal = ({ isOpen, toggleFunction, title, message }) => {
  return (
    <Modal isOpen={isOpen} toggle={toggleFunction}>
      <ModalHeader>{title}</ModalHeader>
      <ModalBody>
        <div>
          <h6>
            <b>Thank you for the transaction</b>
          </h6>
          <div>Your approval code is: {message?.approvalCode}</div>
          <div>Your transaction date is: {message?.dateTime}</div>
        </div>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggleFunction}>
          Close
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default DialogModal;
